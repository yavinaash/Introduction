# GET TO KNOW YOUR ONLY NAASH :heart_eyes: :heart_eyes:
## 1. Biodata 

<img src="https://gitlab.com/yavinaash/Introduction/-/raw/main/me.png" width=150 align=middle>

Take note of my _biodata_ if you would like to **connect** with me 


- Name: **Yavinaash Naidu Saravanakumar**
- Age: ~~21~~ 22 
- From: Klang, Selangor :flag_my: 
- Course: B.Eng Aerospace :airplane: 
- :iphone: :+6012-774 1399
- Fav music: https://www.youtube.com/watch?v=oRdxUFDoQe0
- Fav quote:
> You will face many defeats in life, but never let yourself be defeated.



## 2. Strength and Weaknesses 

| STRENGTH | WEAKNESS |
| ------ | ------ |
| Confidence | Good food  |
| Communication| Emotional |
| Perfectionist | Perfectionist |
| Good friends and family | Memes |


## 3. To-Do List 
- [:heavy_check_mark:] Submit Assignment 1
- [:heavy_check_mark: ] Submit Assignment 2 
- [:heavy_check_mark: ] Submit Assignment 3
- [:heavy_check_mark: ] Submit Assignment 4
- [:heavy_check_mark: ] Submit Assignment 5 
- [:heavy_check_mark: ] Submit Assignment 6

